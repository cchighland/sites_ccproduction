<?php

/**
 * @file
 * This file provides theme override functions for the ccninja theme.
 */

/**
 * Implements hook_template_preprocess_html().
 *
 * Preprocess variables for html.tpl.php.
 */
function ccninja_preprocess_html(&$variables) {

  $apple_icon_sizes = [60,76,120,152];
  foreach ($apple_icon_sizes as $size) {
    $apple = [
      '#tag' => 'link',
      '#attributes' => [
        'rel' => 'apple-touch-icon',
        'sizes' => "{$size}x{$size}",
        'href' => file_create_url(drupal_get_path('theme', 'ccninja') . "/icons/apple-touch-icon-{$size}x{$size}.png"),
      ],
    ];
    drupal_add_html_head($apple, "apple-touch-icon-{$size}");
  }

  // Add Google Fonts.
  drupal_add_css('//fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic', ['group' => CSS_THEME, 'type' => 'external']);
}
