<?php

/**
 * @file
 * Defines custom tokens for this site.
 */

/**
 * Implements hook_token_info().
 */
function mysite_token_info() {
  $info = [];

  $info['tokens']['node'] = [
    'post_date_path' => [
      'name' => t('Post Date Path'),
      'description' => t('A version of field_service_date suitable for pathauto use.'),
    ],
    'service_date_display' => [
      'name' => t('Service Date Display'),
      'description' => t('A formatted version of field_service_date.'),
    ],
    'service_date_path' => [
      'name' => t('Service Date Path'),
      'description' => t('A version of field_service_date suitable for pathauto use.'),
    ],
  ];
  return $info;
}

/**
 * Implements hook_tokens().
 */
function mysite_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = [];

  if ($type == 'node') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'post_date_path':
          if (!empty($data['node']->created)) {
            $timestamp = $data['node']->created;
            $replacements[$original] = format_date($timestamp, 'custom', 'Y-m-d');
          }
          break;
        case 'service_date_display':
          if (!empty($data['node']->field_service_date)) {
            $timestamp = $data['node']->field_service_date[LANGUAGE_NONE][0]['value'];
            $replacements[$original] = format_date($timestamp, 'custom', 'n/j/Y');
          }
          break;
        case 'service_date_path':
          if (!empty($data['node']->field_service_date)) {
            $timestamp = $data['node']->field_service_date[LANGUAGE_NONE][0]['value'];
            $replacements[$original] = format_date($timestamp, 'custom', 'Y-m-d');
          }
          break;
      }
    }
  }
  return $replacements;
}
